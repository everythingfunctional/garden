module simple_test
    use garden, only: result_t, test_item_t, describe, it, succeed

    implicit none
    private
    public :: test_that_the_framework_works
contains
    function test_that_the_framework_works() result(tests)
        type(test_item_t) :: tests

        tests = describe( &
                "the framework works", &
                [ it("because it can run a test", check_it_works) &
                ])
    end function

    function check_it_works() result(result_)
        type(result_t) :: result_

        result_ = succeed("so it works")
    end function
end module
