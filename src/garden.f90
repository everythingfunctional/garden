module garden
    use veggies, veggies_run_tests => run_tests
    use garden_run_tests_m, only: run_tests
    private :: veggies_run_tests
end module
