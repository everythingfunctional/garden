module garden_run_tests_m
    use iso_varying_string, only: varying_string, assignment(=), operator(//), extract, len
    use veggies, only: veggies_run_tests => run_tests, test_item_t
    use veggies_utilities_m, only: &
            current_image, num_imgs, all_image_reports, any_image_failed
            
    implicit none
    private
    public :: run_tests
contains
    function run_tests(tests) result(passed)
        type(test_item_t), intent(in) :: tests
        logical :: passed
        
        current_image = this_image()
        num_imgs = num_images()
        all_image_reports => garden_all_image_reports
        any_image_failed => garden_any_image_failed
        passed = veggies_run_tests(tests)
    end function
    
    function garden_all_image_reports(current_image_report)
        type(varying_string), intent(in) :: current_image_report
        type(varying_string) :: garden_all_image_reports
        
        integer :: max_report_len, i
        integer, allocatable :: report_len[:]
        character(len=:), allocatable :: report_channel[:]

        allocate(report_len[*])
        report_len = len(current_image_report)

        max_report_len = report_len
        call co_max(max_report_len)

        allocate(character(len=max_report_len) :: report_channel[*])
        if (current_image /= 1) report_channel = current_image_report
        sync all

        if (current_image == 1) then
            garden_all_image_reports = current_image_report
            do i = 2, num_imgs
                garden_all_image_reports = &
                        garden_all_image_reports // new_line('a') &
                        // extract(report_channel[i], 1, report_len[i])
            end do
        else
            garden_all_image_reports = ""
        end if
    end function

    function garden_any_image_failed(current_image_failed)
        logical, intent(in) :: current_image_failed
        logical :: garden_any_image_failed

        garden_any_image_failed = current_image_failed
        call co_any(garden_any_image_failed)
    end function
    
    subroutine co_any(x)
        logical, intent(inout) :: x

        call co_reduce(x, or_)
    contains
        pure function or_(lhs, rhs)
            logical, intent(in) :: lhs, rhs
            logical :: or_

            or_ = lhs .or. rhs
        end function
    end subroutine
end module
