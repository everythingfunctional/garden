Garden
==========

[![pipeline status](https://gitlab.com/everythingfunctional/garden/badges/main/pipeline.svg)](https://gitlab.com/everythingfunctional/garden/-/commits/main)

For a healthier code base, eat your vegetables.

Garden is simply layered on top of veggies to enable the testing of code that makes use of Fortran's native parallel features.
The use is exactly the same as for [veggies], simply replacing any occurrence of `veggies` with `garden`.
See the veggies documentation for additional information.

[veggies]: https://gitlab.com/everythingfunctional/veggies
